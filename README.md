# README #

Lerning Javasript properly working files

### The Plant to attainment ###

Weeks 1 and 2
- Introduction
- Data Types
- Expressions
- Operators

1) 	Web Fundamentals Track on Codecademy.
2)	Preface and Chapters 1 and 2 of JavaScript: The Definitive Guide.
	Or Introduction and Chapters 1 and 2 of Professional JavaScript for Web Developers. 
3) 	 Introduction to JavaScript section of the JavaScript Track on Codecademy.
4)	chapters 3 and 4 of JavaScript: The Definitive Guide. 
	Or Preface and Chapters 3 and 4 of Professional JavaScript for Web Developers. 
	Skip the section on �Bitwise Operators�
5)	chapter 5 of JavaScript: The Definitive Guide.
7)	sections 2 to 5 of the JavaScript Track on Codecademy.


Weeks 3 and 4 
- Objects
- Arrays
- Functions
- DOM
- jQuery

1)	Read JavaScript Objects in Detail (http://javascriptissexy.com/javascript-objects-in-detail/)
	Or read chapter 6 of JavaScript: The Definitive Guide.
	Or read chapter 6 of Professional JavaScript for Web Developers. NOTE: Only read the �Understanding Objects� section.
2)  Chapters 7 and 8 of JavaScript: The Definitive Guide
	or chapters 5 and 7 of Professional JavaScript for Web Developers
4)	Codecademy and complete the JavaScript track by working through sections 6, 7, and 8 (Data Structures to Object 2). 
5)	on Codecademy, build the 5 little Basic Projects on the Projects track.
6)	Read chapters 13, 15, 16, and 19 of JavaScript: The Definitive Guide.
	or Or read chapters 8, 9, 10, 11, 13, and 14 of Professional JavaScript for Web Developers and Follow this jQuery course�it: http://try.jquery.com/
	And you can read chapter 19 of JavaScript: The Definitive Guide, if you have the book, for more on jQuery. 
7) 	Work through the entire jQuery course at http://try.jquery.com/


Your First Project�A Dynamic Quiz
- building a JavaScript quiz application as per http://javascriptissexy.com/how-to-learn-javascript-properly/


Weeks 5 and 6 
- Regular Expressions
- Window Object
- Events
- jQuery

1)	Read chapters 10, 14, 17, and 20 of JavaScript: The Definitive Guide.
	Or read chapters 20 and 23 of Professional JavaScript for Web Developers.
4) Improve Your Quiz Application From Earlier:
		- client-side data validation: 
		- back button
		-  jQuery to add animation
		-  Store the quiz questions in an external JSON file.
		- Add user authentication: allow users log in, and save their login credentials to local storage (HTML5 browser storage)
		- Use cookies to remember the user, and show a �Welcome, First Name� message when the user returns to the quiz.
	

Weeks 7 & 8 
- Classes, 
- Inheritance, 
- more HTML5

1)	Read chapters 9, 18, 21, and 22 of JavaScript the Definitive Guide
	or  blog post, OOP In JavaScript: What You NEED to Know. (http://javascriptissexy.com/oop-in-javascript-what-you-need-to-know/)
	or Or read chapters 6 (�Object Creation� and �Inheritance�) 16, 22, and 24 of Professional JavaScript for Web Developers. 
2) 	Improve Your Quiz Application Even Further:
		- bootstrap
		- add Handlebars.js templating to the quiz.
		- 

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact